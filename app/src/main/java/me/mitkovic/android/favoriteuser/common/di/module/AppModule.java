package me.mitkovic.android.favoriteuser.common.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import me.mitkovic.android.favoriteuser.common.di.scope.PerActivity;
import me.mitkovic.android.favoriteuser.detail.di.DetailModule;
import me.mitkovic.android.favoriteuser.detail.view.DetailActivity;
import me.mitkovic.android.favoriteuser.home.di.MainModule;
import me.mitkovic.android.favoriteuser.home.view.MainActivity;


@Module
public abstract class AppModule {

    @PerActivity
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity mainActivityInjector();

    @PerActivity
    @ContributesAndroidInjector(modules = DetailModule.class)
    abstract DetailActivity detailActivityInjector();

}
