package me.mitkovic.android.favoriteuser.common;

import me.mitkovic.android.favoriteuser.common.api.model.User;

public interface Navigation {

    void viewUser(User user);

}
