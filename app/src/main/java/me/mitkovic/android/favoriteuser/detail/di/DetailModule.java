package me.mitkovic.android.favoriteuser.detail.di;

import android.content.Intent;

import dagger.Module;
import dagger.Provides;
import me.mitkovic.android.favoriteuser.common.api.UsersAPI;
import me.mitkovic.android.favoriteuser.common.di.scope.PerActivity;
import me.mitkovic.android.favoriteuser.common.util.LocalStorage;
import me.mitkovic.android.favoriteuser.detail.view.DetailActivity;
import me.mitkovic.android.favoriteuser.detail.view.DetailPresenter;
import me.mitkovic.android.favoriteuser.detail.view.DetailPresenterImpl;


@PerActivity
@Module
public abstract class DetailModule {

    @Provides
    @PerActivity
    public static DetailPresenter providesAuctionPresenter(UsersAPI usersAPI, DetailActivity detailActivity, LocalStorage localStorage) {
        Intent intent = detailActivity.getIntent();
        final int userId = DetailActivity.getUserId(intent);
        return new DetailPresenterImpl(usersAPI, userId, localStorage);
    }

}
