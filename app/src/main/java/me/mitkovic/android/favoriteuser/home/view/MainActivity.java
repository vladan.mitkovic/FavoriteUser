package me.mitkovic.android.favoriteuser.home.view;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import me.mitkovic.android.favoriteuser.R;

import me.mitkovic.android.favoriteuser.common.api.model.User;

public class MainActivity extends AppCompatActivity implements MainView, UserRow.Listener {

    @Inject
    MainPresenter presenter;

    @BindView(R.id.users_list)
    RecyclerView recyclerView;

    @Inject
    UsersListAdapter usersListAdapter;

    @BindView(R.id.loading)
    View loading;

    @BindView(R.id.layout_holder)
    RelativeLayout layoutHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        usersListAdapter.setListener(this);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(usersListAdapter);

        if (savedInstanceState != null) {
            presenter.onRestore(savedInstanceState);
        }
        presenter.onBind(this);
    }

    @Override
    public void onClickUser(User user) {
        presenter.onClickUser(user);
    }

    @Override
    public void setAdapterData(List<User> user) {
        usersListAdapter.setUsers(user);
    }

    @Override
    public void setSuccessSnackBar() {
        String successText = getString(R.string.success_message);
        Snackbar snackbar = Snackbar.make(layoutHolder, successText, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void setErrorMessage(String errorMessage) {
        Toast.makeText(MainActivity.this, getString(R.string.error_users_message) + errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setLoading(boolean isLoading) {
        loading.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (usersListAdapter != null) {
            usersListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (presenter != null)
            presenter.onSave(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null)
            presenter.onUnbind();
    }
}
