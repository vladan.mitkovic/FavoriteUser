package me.mitkovic.android.favoriteuser.home.view;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import me.mitkovic.android.favoriteuser.common.api.model.User;
import me.mitkovic.android.favoriteuser.common.util.LocalStorage;

public class UsersListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private UserRow.Listener listener;
    private LocalStorage localStorage;
    private List<User> users = new ArrayList<>();

    @Inject
    public UsersListAdapter(LocalStorage localStorage) {
        this.localStorage = localStorage;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserViewHolder(new UserRow(parent.getContext(), localStorage), listener);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserViewHolder) {
            ((UserViewHolder) holder).bind(users.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void setUsers(List<User> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    public void setListener(UserRow.Listener listener) {
        this.listener = listener;
    }

}
