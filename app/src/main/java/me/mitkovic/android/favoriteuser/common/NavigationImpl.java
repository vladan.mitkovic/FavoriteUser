package me.mitkovic.android.favoriteuser.common;

import android.app.Activity;

import me.mitkovic.android.favoriteuser.common.api.model.User;
import me.mitkovic.android.favoriteuser.detail.view.DetailActivity;

public class NavigationImpl implements Navigation {

    private final Activity activity;

    public NavigationImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void viewUser(User user) {
        activity.startActivity(DetailActivity.intent(activity, user.getId()));
    }

}
