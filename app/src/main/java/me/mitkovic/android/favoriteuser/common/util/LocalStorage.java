package me.mitkovic.android.favoriteuser.common.util;

public interface LocalStorage {

    void addFavoriteUser(int userId);

    void removeFavoriteUser(int userId);

    boolean hasFavoriteUser(int userId);

}
