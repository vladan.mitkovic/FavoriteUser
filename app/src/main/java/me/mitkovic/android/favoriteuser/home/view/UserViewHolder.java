package me.mitkovic.android.favoriteuser.home.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import me.mitkovic.android.favoriteuser.common.api.model.User;

public class UserViewHolder extends RecyclerView.ViewHolder {

    UserRow itemView;

    public UserViewHolder(final View itemView, final UserRow.Listener listener) {
        super(itemView);
        this.itemView = (UserRow) itemView;
        this.itemView.setListener(listener);
    }

    public void bind(final User user) {
        itemView.setUser(user);
    }
}
