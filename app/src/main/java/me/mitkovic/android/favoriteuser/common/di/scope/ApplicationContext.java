package me.mitkovic.android.favoriteuser.common.di.scope;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
