package me.mitkovic.android.favoriteuser.common.api;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import me.mitkovic.android.favoriteuser.common.api.model.User;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface UsersAPI {

    @GET("getuserslist")
    Observable<List<User>> getUsers();

    @GET("getuserslist/{userId}")
    Single<User> getUser(@Path("userId") int userId);

}
