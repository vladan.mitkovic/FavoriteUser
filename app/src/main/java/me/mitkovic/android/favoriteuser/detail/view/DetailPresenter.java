package me.mitkovic.android.favoriteuser.detail.view;

import me.mitkovic.android.favoriteuser.common.view.Presenter;

public interface DetailPresenter extends Presenter<DetailView> {

    void onClickAddFavorite();

}
