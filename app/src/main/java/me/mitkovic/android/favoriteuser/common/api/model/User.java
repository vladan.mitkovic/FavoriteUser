package me.mitkovic.android.favoriteuser.common.api.model;

public class User {

    private int id;
    private String first_name;
    private String last_name;
    private String profile_picture_small;
    private String profile_picture_big;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public String getProfilePictureSmall() {
        return profile_picture_small;
    }

    public String getProfilePictureBig() {
        return profile_picture_big;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", profile_picture_small='" + profile_picture_small + '\'' +
                ", profile_picture_big='" + profile_picture_big + '\'' +
                '}';
    }
}
