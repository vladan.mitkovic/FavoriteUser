package me.mitkovic.android.favoriteuser.home.view;

import java.util.List;

import me.mitkovic.android.favoriteuser.common.api.model.User;
import me.mitkovic.android.favoriteuser.common.view.View;

public interface MainView extends View {

    void setAdapterData(List<User> users);

    void setSuccessSnackBar();

    void setErrorMessage(String errorMessage);

    void setLoading(boolean isLoading);

}
