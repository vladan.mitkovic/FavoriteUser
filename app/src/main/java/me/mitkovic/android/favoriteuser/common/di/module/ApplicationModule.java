package me.mitkovic.android.favoriteuser.common.di.module;

import android.app.Activity;
import android.content.Context;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import me.mitkovic.android.favoriteuser.common.FavoriteUserApplication;
import me.mitkovic.android.favoriteuser.common.di.scope.ApplicationContext;
import me.mitkovic.android.favoriteuser.common.di.scope.FavoriteUserApplicatonScope;
import me.mitkovic.android.favoriteuser.common.util.LocalStorage;
import me.mitkovic.android.favoriteuser.common.util.SharedPreferencesLocalStorage;

@Module
public abstract class ApplicationModule {

    @FavoriteUserApplicatonScope
    @Binds
    abstract AndroidInjector<Activity> bindsActivityInjector(DispatchingAndroidInjector<Activity> injector);

    @Provides
    @FavoriteUserApplicatonScope
    @ApplicationContext
    public static Context providesContext(FavoriteUserApplication application) {
        return application.getApplicationContext();
    }

    @Provides
    @FavoriteUserApplicatonScope
    public static LocalStorage providesLocalStorage(@ApplicationContext Context context) {
        return new SharedPreferencesLocalStorage(context);
    }

}
