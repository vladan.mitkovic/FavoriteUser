package me.mitkovic.android.favoriteuser.home.di;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import me.mitkovic.android.favoriteuser.common.Navigation;
import me.mitkovic.android.favoriteuser.common.NavigationImpl;
import me.mitkovic.android.favoriteuser.common.di.scope.PerActivity;
import me.mitkovic.android.favoriteuser.home.view.MainActivity;
import me.mitkovic.android.favoriteuser.home.view.MainPresenter;
import me.mitkovic.android.favoriteuser.home.view.MainPresenterImpl;


@PerActivity
@Module
public abstract class MainModule {

    @Binds
    abstract MainPresenter bindsMainPresenter(MainPresenterImpl presenter);

    @PerActivity
    @Provides
    public static Navigation providesNavigation(MainActivity mainActivity) {
        return new NavigationImpl(mainActivity);
    }

}
