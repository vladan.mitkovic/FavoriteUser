package me.mitkovic.android.favoriteuser.detail.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import me.mitkovic.android.favoriteuser.R;
import me.mitkovic.android.favoriteuser.common.api.model.User;

public class DetailActivity extends AppCompatActivity implements DetailView {

    private static final String ARG_USER_ID = "USER_ID";

    @Inject
    DetailPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.user_avatar)
    SimpleDraweeView thumbnail;

    @BindView(R.id.loading)
    View loading;

    @BindView(R.id.layout_holder)
    ScrollView layoutHolder;

    @BindView(R.id.add_to_favorites)
    Button addToFavorites;

    @BindView(R.id.favorite_ico)
    ImageView favoriteIco;

    public static Intent intent(Context context, int userId) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(ARG_USER_ID, userId);
        return intent;
    }

    public static Integer getUserId(Intent userIntent) {
        if (userIntent.hasExtra(ARG_USER_ID)) {
            return userIntent.getIntExtra(ARG_USER_ID, 0);
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (savedInstanceState != null) {
            presenter.onRestore(savedInstanceState);
        }
        presenter.onBind(this);
    }

    @OnClick(R.id.add_to_favorites)
    void onClickAddFavorite() {
        presenter.onClickAddFavorite();
    }

    @Override
    public void setUserData(User user) {
        toolbar.setTitle(user.getFirstName() != null ?
                user.getFirstName() + " " + user.getLastName() : user.getFirstName());

        thumbnail.setImageURI(user.getProfilePictureBig());
    }

    @Override
    public void setFavoriteText(boolean isFavorite) {
        addToFavorites.setText(isFavorite == true ?
                getString(R.string.remove_from_favorites) : getString(R.string.add_to_favorites));
    }

    @Override
    public void setFavoriteIco(boolean isFavorite) {
        favoriteIco.setVisibility(isFavorite ?
                View.VISIBLE : View.GONE);
    }

    @Override
    public void setSuccessSnackBar() {
        String successText = getString(R.string.success_message);
        Snackbar snackbar = Snackbar.make(layoutHolder, successText, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void setErrorMessage(String errorMessage) {
        Toast.makeText(DetailActivity.this, getString(R.string.error_user_message) + errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setLoading(boolean isLoading) {
        loading.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (presenter != null)
            presenter.onSave(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null)
            presenter.onUnbind();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
