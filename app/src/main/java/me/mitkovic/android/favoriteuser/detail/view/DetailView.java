package me.mitkovic.android.favoriteuser.detail.view;

import me.mitkovic.android.favoriteuser.common.api.model.User;
import me.mitkovic.android.favoriteuser.common.view.View;

public interface DetailView extends View {

    void setUserData(User user);

    void setSuccessSnackBar();

    void setErrorMessage(String errorMessage);

    void setLoading(boolean isLoading);

    void setFavoriteText(boolean isFavorite);

    void setFavoriteIco(boolean isFavorite);

}
