package me.mitkovic.android.favoriteuser.detail.view;

import android.os.Bundle;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import icepick.Icepick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.mitkovic.android.favoriteuser.common.api.UsersAPI;
import me.mitkovic.android.favoriteuser.common.api.model.User;
import me.mitkovic.android.favoriteuser.common.util.LocalStorage;

public class DetailPresenterImpl implements DetailPresenter {

    private DetailView view;
    private UsersAPI usersAPI;
    private int userId;
    private LocalStorage localStorage;
    private boolean isLoading;

    private CompositeDisposable subscription;

    @Inject
    public DetailPresenterImpl(UsersAPI usersAPI, int userId, LocalStorage localStorage) {
        this.usersAPI = usersAPI;
        this.userId = userId;
        this.localStorage = localStorage;
    }

    @Override
    public void onBind(DetailView view) {
        this.view = view;
        setLoading(true);

        subscription = new CompositeDisposable();

        view.setFavoriteText(localStorage.hasFavoriteUser(userId));
        view.setFavoriteIco(localStorage.hasFavoriteUser(userId));

        getData();
    }

    private void getData() {
        Disposable serviceSubscription = usersAPI.getUser(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successConsumer(), errorConsumer());

        subscription.add(serviceSubscription);
    }

    private Consumer<User> successConsumer() {
        return new Consumer<User>() {

            @Override
            @DebugLog
            public void accept(final User user) throws Exception {
                setLoading(false);
                view.setSuccessSnackBar();
                view.setUserData(user);
            }
        };
    }

    private Consumer<Throwable> errorConsumer() {
        return new Consumer<Throwable>() {

            @Override
            @DebugLog
            public void accept(Throwable throwable) throws Exception {
                setLoading(false);
                view.setErrorMessage(throwable.getMessage());
            }
        };
    }

    @Override
    public void onClickAddFavorite() {
        if (isLoading) return;

        boolean isFavorite;
        if (localStorage.hasFavoriteUser(userId)) {
            isFavorite = false;
            localStorage.removeFavoriteUser(userId);
        } else {
            isFavorite = true;
            localStorage.addFavoriteUser(userId);
        }
        view.setFavoriteText(isFavorite);
        view.setFavoriteIco(isFavorite);
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
        view.setLoading(loading);
    }

    @Override
    public void onUnbind() {
        this.view = null;

        if (subscription != null) {
            subscription.dispose();
            subscription = null;
        }
    }

    @Override
    public void onRestore(Bundle bundle) {
        Icepick.restoreInstanceState(this, bundle);
    }

    @Override
    public void onSave(Bundle bundle) {
        Icepick.saveInstanceState(this, bundle);
    }

}
