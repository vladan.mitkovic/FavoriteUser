package me.mitkovic.android.favoriteuser.home.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.mitkovic.android.favoriteuser.R;
import me.mitkovic.android.favoriteuser.common.api.model.User;
import me.mitkovic.android.favoriteuser.common.util.LocalStorage;

public class UserRow extends FrameLayout {

    @BindView(R.id.user_avatar)
    SimpleDraweeView thumbnail;

    @BindView(R.id.favorite_ico)
    ImageView favoriteIco;

    private User user;
    private LocalStorage localStorage;

    public interface Listener {
        void onClickUser(User user);
    }

    public UserRow(final Context context, LocalStorage localStorage) {
        super(context);
        initialize(context);
        this.localStorage = localStorage;
    }

    public UserRow(final Context context, @Nullable final AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    private void initialize(final Context context) {
        inflate(context, R.layout.list_item_user, this);

        setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        ButterKnife.bind(this, this);
    }

    public void setListener(final Listener listener) {
        setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClickUser(user);
            }
        });
    }

    public void setUser(final User user) {
        this.user = user;

        thumbnail.setImageURI(user.getProfilePictureSmall());
        favoriteIco.setVisibility(localStorage.hasFavoriteUser(user.getId()) == true ? View.VISIBLE : View.GONE);
    }

}
