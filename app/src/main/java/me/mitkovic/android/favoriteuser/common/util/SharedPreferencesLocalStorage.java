package me.mitkovic.android.favoriteuser.common.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesLocalStorage implements LocalStorage {

    private static final String PREFERENCES_DEFAULT_NAME = "FAVORITE_USER_SHARED_PREFS";
    private static final String KEY_FAVORITE_USER = "FAVORITE_USER_";

    private final SharedPreferences defaultPreferences;

    public SharedPreferencesLocalStorage(Context context) {
        defaultPreferences = context.getSharedPreferences(PREFERENCES_DEFAULT_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public void addFavoriteUser(int userId) {
        defaultPreferences.edit()
                .putInt(KEY_FAVORITE_USER + userId, userId)
                .apply();
    }

    @Override
    public void removeFavoriteUser(int userId) {
        defaultPreferences
                .edit()
                .remove(KEY_FAVORITE_USER + userId)
                .apply();
    }

    @Override
    public boolean hasFavoriteUser(int userId) {
        return defaultPreferences.contains(KEY_FAVORITE_USER + userId);
    }

}
