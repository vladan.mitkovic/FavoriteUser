package me.mitkovic.android.favoriteuser.common.di;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import me.mitkovic.android.favoriteuser.common.FavoriteUserApplication;
import me.mitkovic.android.favoriteuser.common.di.module.APIModule;
import me.mitkovic.android.favoriteuser.common.di.module.AppModule;
import me.mitkovic.android.favoriteuser.common.di.module.ApplicationModule;
import me.mitkovic.android.favoriteuser.common.di.scope.FavoriteUserApplicatonScope;

@FavoriteUserApplicatonScope
@Component(modules = {
        AndroidInjectionModule.class,
        ApplicationModule.class,
        APIModule.class,
        AppModule.class
})
public interface ApplicationComponent {

    void inject(FavoriteUserApplication peopleApplication);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(FavoriteUserApplication application);
        ApplicationComponent build();
    }

}
