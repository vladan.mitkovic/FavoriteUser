package me.mitkovic.android.favoriteuser.home.view;

import me.mitkovic.android.favoriteuser.common.api.model.User;
import me.mitkovic.android.favoriteuser.common.view.Presenter;

public interface MainPresenter extends Presenter<MainView> {

    void onClickUser(User user);

}
